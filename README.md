# Pretest
Framework ini bisa digunakan oleh kandidat yang mendaftarkan diri 
ke **PT Renseki Angkasa Jaya** dalam menyelesaikan sekumpulan problem 
yang diberikan sebagai bentuk screening awal posisi Software Developer di Renseki.

Dikarenakan hanya bersifat membantu, kandidat tidak harus menggunakan project
ini untuk menjawab problem yang telah diberikan.

Problem terletak pada folder `questions/test`. Lihatlah problem 
`ExampleQuestion.md` sebagai contoh penggunaan framework ini.
IDE yang disarankan adalah `IntelliJ`. 

Solusi bisa dikerjakan dengan bahasa `Java` atau `Kotlin`. Bilamana kurang 
familiar dengan kedua bahasa tersebut, kandidat boleh menuliskan solusinya
dengan pilihan bahasa sebagai berikut:
- C#
- C++
- Go
- Basic
- Javascript
- PHP
- Python
- Swift

## Submission
Solusi dikirimkan melalui email ke 
[erick.pranata@renseki.com](mailto:erick.pranata@renseki.com)
dalam sebuah folder yang sudah di-_compress_.

Usahakan agar file yang dikirimkan hanya berisi source code solusi Anda,
agar ukuran file tidak terlalu besar.

## Pertanyaan? 
Hubungi Erick Pranata di [erick.pranata@renseki.com](mailto:erick.pranata@renseki.com)
bilamana ada yang perlu ditanyakan terkait test ini.